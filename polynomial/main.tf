module "multiply" {
  source = "../modules/product"
  a = 12
  b = 10
}

module "add" {
  source = "../modules/sum"
  a = "${module.multiply.result}"
  b = 4
}

locals {
  total = "${module.add.result}"
}

output "total" {
  value = "${local.total}"
}

resource "null_resource" "result" {
  triggers = {
    total = "${local.total}"
  }
}
